import { Module } from '@nestjs/common';
import { TicketsService } from './tickets.service';
import { TicketsController } from './tickets.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Tickets } from 'src/entities/tickets.entity';
import { Status } from 'src/entities/status.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Tickets, Status])],
  controllers: [TicketsController],
  providers: [TicketsService],
})
export class TicketsModule { }
