/* eslint-disable prettier/prettier */
import { BadRequestException, Injectable, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { CreateTicketDto } from './dto/create-ticket.dto';
import { UpdateTicketDto } from './dto/update-ticket.dto';
import { AppDataSource } from 'src/config/database.conf';
import { InjectRepository } from '@nestjs/typeorm';
import { Tickets } from 'src/entities/tickets.entity';
import { Not, Repository } from 'typeorm';
import { Status } from 'src/entities/status.entity';

interface IQueryOption { sort?: 'DESC' | 'ASC', page?: number, limit?: number, condition?: ISearch }
export interface ISearch { keyword?: string, status?: number }

@Injectable()
export class TicketsService {
  protected readonly defaultLimit: number = 10;
  protected readonly defaultSort: 'ASC' | 'DESC' = 'DESC';
  protected data: any

  constructor(
    @InjectRepository(Tickets)
    private _ticketsRepo: Repository<Tickets>,

    @InjectRepository(Status)
    private _statusRepo: Repository<Status>,

  ) { }

  async findAll(options?: IQueryOption) {
    const page = 'page' ? options.page ?? 1 : 1;
    const limit = 'limit' ? options.limit ?? this.defaultLimit : this.defaultLimit;
    const condition = 'condition' ? options.condition : {};
    const offset = (page - 1) * limit;

    const query = this._ticketsRepo
      .createQueryBuilder("ticket")
      .select([
        'ticket.id AS id',
        'ticket.title AS ticket_title',
        'ticket.description AS ticket_description',
        'ticket.contact AS ticket_contact',
        'ticket.information AS ticket_information',
        'ticket.created_at AS ticket_created_at',
        'ticket.updated_at AS ticket_updated_at',
        'ticket.status_id AS ticket_status_id',
        'status.id AS status_id',
        'status.title AS status_title',
      ])
      .innerJoin('ticket.status', 'status')

    if (condition?.keyword) {
      query.andWhere("ticket.title like :keyword", { keyword: `%${condition.keyword}%` });
    }

    if (condition?.status) {
      query.andWhere("ticket.status_id = :status", { status: condition.status });
    }

    const total = await query.getCount();
    this.data = await
      query.skip(offset)
        .orderBy('(CASE WHEN ticket_updated_at IS NULL THEN ticket_created_at ELSE ticket_updated_at END)', 'DESC')
        .take(limit)
        .getRawMany();

    return { total, page, limit, condition, result: this.data }
  }

  async findOne(id: number) {
    const result = await this._ticketsRepo
      .createQueryBuilder('ticket')
      .select([
        'ticket.id AS id',
        'ticket.title AS title',
        'ticket.description AS description',
        'ticket.contact AS contact',
        'ticket.information AS information',
        'ticket.status_id AS status',
      ])
      .where('ticket.id = :id', { id: id })
      .getRawOne();
    if (!result) throw new NotFoundException()
    return result
  }

  async findStatus() {
    const result = await this._statusRepo.find()
    if (!result) throw new NotFoundException()
    return result
  }

  async create(req: CreateTicketDto) {
    const checkName = await this._ticketsRepo.count({ where: { title: req.title } })
    if (checkName > 0) throw new BadRequestException({ msg: "Title is Duplicate" })

    const dataStore = {
      ...req,
      created_at: new Date()
    }

    const qr = AppDataSource.createQueryRunner();
    await qr.connect();
    await qr.startTransaction();
    let result: any;
    try {
      result = await this._ticketsRepo.save(dataStore)
      await qr.commitTransaction()
      return result
    } catch (err) {
      await qr.rollbackTransaction()
      throw new InternalServerErrorException();
    }
  }

  async update(id: number, req: UpdateTicketDto) {
    const findId = await this._ticketsRepo.findOne({ where: { id } });
    if (!findId) throw new NotFoundException();

    const checkName = await this._ticketsRepo.count({
      where: {
        title: req.title,
        id: Not(id),
      }
    })
    if (checkName > 0) throw new BadRequestException({ msg: "Title is Duplicate" })

    const updateStore = {
      ...req,
      updated_at: new Date()
    }

    const qr = AppDataSource.createQueryRunner();
    await qr.connect();
    await qr.startTransaction();
    let result: any;
    try {
      result = await this._ticketsRepo.update({ id: id }, updateStore)
      await qr.commitTransaction()
      return result
    } catch (err) {
      await qr.rollbackTransaction()
      throw new InternalServerErrorException();
    }
  }

  async remove(id: number) {
    const findId = await this._ticketsRepo.findOne({ where: { id } });
    if (!findId) throw new NotFoundException();

    const qr = AppDataSource.createQueryRunner();
    await qr.connect();
    await qr.startTransaction();
    let result: any;
    try {
      result = await this._ticketsRepo.delete({ id: id })
      await qr.commitTransaction()
      return result
    } catch (err) {
      await qr.rollbackTransaction()
      throw new InternalServerErrorException();
    }
  }

}
