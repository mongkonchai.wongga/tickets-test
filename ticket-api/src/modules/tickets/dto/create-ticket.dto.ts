/* eslint-disable prettier/prettier */

import { IsNotEmpty, IsNumber } from "class-validator";

export class CreateTicketDto {

    @IsNotEmpty()
    readonly title: string

    @IsNotEmpty()
    readonly description: string

    @IsNotEmpty()
    readonly contact: string

    @IsNotEmpty()
    readonly information: string

    @IsNotEmpty()
    @IsNumber()
    readonly status_id: number;

}
