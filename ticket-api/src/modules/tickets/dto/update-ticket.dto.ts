/* eslint-disable prettier/prettier */
import { CreateTicketDto } from './create-ticket.dto';

export class UpdateTicketDto extends CreateTicketDto { }
