/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { AppService } from './app.service';
import { TicketsModule } from 'src/modules/tickets/tickets.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeormConfig } from 'src/config/database.conf';

@Module({
  imports: [
    TicketsModule,
    TypeOrmModule.forRoot({ ...typeormConfig })
  ],
  controllers: [],
  providers: [AppService],
})
export class AppModule { }
