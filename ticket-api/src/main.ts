import { NestFactory } from '@nestjs/core';
import { AppModule } from './app/app.module';
import { Logger } from '@nestjs/common';

async function bootstrap() {
  const nodeEnv = process.env.NODE_ENV;
  const apiPort = parseInt(process.env.API_PORT) || 3000;
  const app = await NestFactory.create(AppModule);
  app.enableCors({
    origin: '*',
    allowedHeaders: '*',
    methods: 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
  });

  await app.listen(apiPort, () => {
    Logger.log(`Start with port ${apiPort} ---> environment is ${nodeEnv}`);
  });
}

bootstrap();
