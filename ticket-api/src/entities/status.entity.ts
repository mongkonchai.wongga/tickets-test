/* eslint-disable prettier/prettier */
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Tickets } from './tickets.entity';
@Entity('status')
export class Status {
    @PrimaryGeneratedColumn() id: number;
    @Column() title: string;

    @OneToMany(() => Tickets, ticket => ticket.status)
    tickets: Tickets[];
}
