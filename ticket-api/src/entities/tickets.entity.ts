/* eslint-disable prettier/prettier */
import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { Status } from "./status.entity";
@Entity('tickets')
export class Tickets {
    @PrimaryGeneratedColumn() id: number
    @Column({ type: 'varchar', length: 255 }) title: string
    @Column({ type: 'text' }) description: string
    @Column({ type: 'text' }) contact: string
    @Column({ type: 'text' }) information: string

    @CreateDateColumn({
        type: 'timestamp',
        name:'created_at'
    })
    created_at: Date

    @UpdateDateColumn({
        name:'updated_at',
        type: 'timestamp',
        nullable: true,
        default: () => null,
        onUpdate: "CURRENT_TIMESTAMP(6)"
    })
    updated_at: Date;

    @ManyToOne(() => Status, status => status.tickets, { nullable: false })
    @JoinColumn({ name: 'status_id', referencedColumnName: 'id' })
    status: Status;

}
