import {
  Button,
  Container,
  Grid,
  Paper,
  TextField,
  Typography,
  makeStyles,
} from "@material-ui/core";
import { DataGrid } from "@material-ui/data-grid";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { ActionSaga } from "../services/action.saga";
import { TicketAction } from "../stores/ticket/ticket.action";
import { IStates } from "../stores/root.reducer";
import { useFormik } from "formik";
import FilterCommon from "../component/filter";
import AddIcon from "@material-ui/icons/Add";
import { useNavigate } from "react-router-dom";

const useStyles = makeStyles({
  root: {
    height: 400,
    width: "100%",
    display: "flex",
    marginTop: "50px",
    textAlign: "center",
  },
});

const ListTicket = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { list }: any = useSelector((state: IStates) => state.ticketReducer);
  const navigate = useNavigate();

  useEffect(() => {
    dispatch(ActionSaga({ type: TicketAction.TICKET_LIST_R }));
  }, [dispatch]);

  const handleCreate = () => {
    navigate("/form");
  };

  const columns: any = [
    {
      field: "id",
      headerName: "ID",
      width: 120,
      editable: false,
    },
    {
      field: "ticket_title",
      headerName: "Title",
      width: 160,
      editable: false,
    },
    {
      field: "ticket_description",
      headerName: "Description",
      width: 160,
      editable: false,
    },
    {
      field: "ticket_contact",
      headerName: "Contact",
      width: 160,
      editable: false,
    },
    {
      field: "ticket_information",
      headerName: "Information",
      sortable: false,
      width: 160,
    },
    {
      field: "status_title",
      headerName: "Status",
      sortable: false,
      width: 160,
    },
    {
      field: "actions",
      headerName: "Actions",
      sortable: false,
      width: 200,
      renderCell: (params: any) => {
        const { id } = params.row;
        const handleEdit = () => {
          navigate(`/form/${id}`);
        };
        return (
          <Button onClick={handleEdit} variant="contained">
            Edit
          </Button>
        );
      },
    },
  ];

  const formik = useFormik({
    initialValues: {
      keyword: "",
      status: -1,
    },
    onSubmit: (values: any) => {
      let payload: any = { page: 1, limit: 5 };
      if (values.keyword.length > 0) payload.keyword = values.keyword;
      if (Number(values.status) !== -1) payload.status = values.status;

      dispatch(
        ActionSaga({
          type: TicketAction.TICKET_LIST_R,
          payload: payload,
        })
      );
    },

    onReset: () => {
      dispatch(
        ActionSaga({
          type: TicketAction.TICKET_LIST_R,
          payload: { page: 1, limit: 5 },
        })
      );
    },
  });

  const renderFilter = () => {
    return (
      <form onSubmit={formik.handleSubmit} onReset={formik.handleReset}>
        <FilterCommon visionFilter={true} onSearch={formik.handleSubmit}>
          <Grid item sm={6}>
            <TextField
              fullWidth
              size="small"
              name="keyword"
              label="keyword"
              placeholder="keyword"
              variant="outlined"
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              onChange={formik.handleChange}
              value={formik.values.keyword}
            />
          </Grid>

          <Grid item sm={6}>
            <TextField
              name="status"
              select
              margin="normal"
              size="small"
              label={`Status`}
              SelectProps={{
                native: true,
              }}
              InputLabelProps={{
                shrink: true,
              }}
              variant="outlined"
              fullWidth
              onChange={formik.handleChange}
              value={formik.values.status}
            >
              <option key={0} value={-1}>
                {"All Status"}
              </option>
              <option key={1} value={1}>
                {"pending"}
              </option>
              <option key={2} value={2}>
                {"accepted"}
              </option>
              <option key={3} value={3}>
                {"resolved"}
              </option>
              <option key={4} value={4}>
                {"rejected"}
              </option>
            </TextField>
          </Grid>
        </FilterCommon>
      </form>
    );
  };

  return (
    <Container>
      <Typography variant="h1">Ticket</Typography>
      {renderFilter()}
      <Grid item sm={12}>
        <Button
          size="large"
          color="primary"
          variant="contained"
          startIcon={<AddIcon />}
          onClick={handleCreate}
        >
          {" "}
          Create{" "}
        </Button>
      </Grid>

      <Paper className={classes.root}>
        <Grid item sm={12}>
          <DataGrid
            rows={list}
            columns={columns}
            pageSize={5}
          />
        </Grid>
      </Paper>
    </Container>
  );
};
export default ListTicket;
