import { makeStyles } from "@material-ui/core/styles";
import {
  Container,
  Paper,
  Button,
  Grid,
  TextField,
  Box,
  Typography,
} from "@material-ui/core";
import { Formik } from "formik";
import * as yup from "yup";
import { useNavigate, useParams } from "react-router-dom";
import { TicketAction } from "../stores/ticket/ticket.action";
import { useEffect } from "react";
import { ActionSaga } from "../services/action.saga";
import { useDispatch } from "react-redux";
import SaveIcon from "@material-ui/icons/Save";
import CloseIcon from "@material-ui/icons/Close";
import { useSelector } from "react-redux";
import { IStates } from "../stores/root.reducer";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    width: "100%",
    height: "auto",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  btnCenter: {
    textAlign: "center",
    "& button": {
      margin: "10px",
      color: "#fff",
    },
  },
}));

const initialValues = {
  title: "",
  description: "",
  contact: "",
  information: "",
  status: 1,
};

const validationSchema = yup.object().shape({
  title: yup.string().required("Title is require"),
  description: yup.string().required("Description is require"),
  contact: yup.string().required("Contact is require"),
  information: yup.string().required("Information is require"),
});

const TicketForm = () => {
  const classes = useStyles();
  const { id }: any = useParams();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { list_status, form }: any = useSelector(
    (state: IStates) => state.ticketReducer
  );
  let called = false;

  useEffect(() => {
    dispatch(
      ActionSaga({
        type: TicketAction.STATUS_LIST_R,
      })
    );

    if (id !== undefined) {
      dispatch(
        ActionSaga({
          type: TicketAction.TICKET_FORM_R,
          payload: id,
        })
      );
    }
  }, [dispatch, id]);

  const handleCancel = () => {
    navigate("/");
  };

  const submitForm = (value: any) => {
    if (id) {
      dispatch(
        ActionSaga({
          type: TicketAction.TICKET_UPDATE,
          payload: {
            id: id,
            title: value.title,
            description: value.description,
            contact: value.contact,
            information: value.information,
            status: value.status,
          },
          onSuccess: (data: any) => {
            alert("Success");
            navigate("/");
          },
          onFailure: (error: any) => {
            alert("Error occurred: " + error.msg);
          },
        })
      );
    } else {
      dispatch(
        ActionSaga({
          type: TicketAction.TICKET_STORE,
          payload: {
            title: value.title,
            description: value.description,
            contact: value.contact,
            information: value.information,
            status: value.status,
          },
          onSuccess: (data: any) => {
            alert("Success");
            navigate("/");
          },
          onFailure: (error: any) => {
            alert("Error occurred: " + error.msg);
          },
        })
      );
    }
  };

  const renderStatus = (data:any) => {
    return list_status.map((item: any, index: number) => (
      <option key={`status_${index}`} value={item.id}>
        {item?.title}
      </option>
    ));
  };

  return (
    <Container>
      <Paper className={classes.paper}>
        <Typography variant="h3">Ticket Form</Typography>
        <Formik
          initialValues={initialValues}
          validateOnChange={false}
          validationSchema={validationSchema}
          onSubmit={(values: any) => submitForm(values)}
        >
          {(props) => {
            const { values, errors, handleChange, handleSubmit, setValues } =
              props;
            if (!called && id && form.id) {
              setValues({
                ...values,
                ...form,
              });
              called = true;
            }

            return (
              <form onSubmit={handleSubmit}>
                <Grid container spacing={3}>
                  <Grid item sm={12}>
                    <TextField
                      name="title"
                      label="Title"
                      placeholder="Title"
                      variant="outlined"
                      fullWidth
                      margin="normal"
                      value={values.title}
                      onChange={handleChange}
                      error={Boolean(errors.title)}
                      helperText={errors.title ? String(errors.title) : ""}
                      InputLabelProps={{
                        shrink: true,
                      }}
                      size="small"
                    />
                  </Grid>
                  <Grid item sm={12}>
                    <TextField
                      name="description"
                      label="Description"
                      placeholder="Description"
                      variant="outlined"
                      fullWidth
                      margin="normal"
                      value={values.description}
                      onChange={handleChange}
                      error={Boolean(errors.description)}
                      helperText={
                        errors.description ? String(errors.description) : ""
                      }
                      InputLabelProps={{
                        shrink: true,
                      }}
                      size="small"
                    />
                  </Grid>
                  <Grid item sm={12}>
                    <TextField
                      name="contact"
                      label="Contact"
                      variant="outlined"
                      placeholder="Contact"
                      fullWidth
                      margin="normal"
                      value={values.contact}
                      onChange={handleChange}
                      error={Boolean(errors.contact)}
                      helperText={errors.contact ? String(errors.contact) : ""}
                      InputLabelProps={{
                        shrink: true,
                      }}
                      size="small"
                    />
                  </Grid>
                  <Grid item sm={12}>
                    <TextField
                      name="information"
                      label="Information"
                      placeholder="Information"
                      variant="outlined"
                      fullWidth
                      margin="normal"
                      value={values.information}
                      onChange={handleChange}
                      error={Boolean(errors.information)}
                      helperText={
                        errors.information ? String(errors.information) : ""
                      }
                      InputLabelProps={{
                        shrink: true,
                      }}
                      size="small"
                    />
                  </Grid>

                  <Grid item sm={12}>
                    <TextField
                      name="status"
                      label="Status"
                      SelectProps={{
                        native: true,
                      }}
                      select
                      variant="outlined"
                      fullWidth
                      margin="normal"
                      value={values.status}
                      onChange={handleChange}
                      error={Boolean(errors.status)}
                      InputLabelProps={{
                        shrink: true,
                        required: true,
                      }}
                      size="small"
                    >
                      {renderStatus(values.status)}
                    </TextField>
                  </Grid>

                  <Grid item md={12}>
                    <Box className={classes.btnCenter}>
                      <Button
                        variant="contained"
                        color="primary"
                        type="submit"
                        startIcon={<SaveIcon />}
                        size="large"
                      >
                        Submit
                      </Button>
                      <Button
                        variant="contained"
                        color="secondary"
                        startIcon={<CloseIcon />}
                        size="large"
                        onClick={handleCancel}
                      >
                        Cancle
                      </Button>
                    </Box>
                  </Grid>
                </Grid>
              </form>
            );
          }}
        </Formik>
      </Paper>
    </Container>
  );
};

export default TicketForm;
