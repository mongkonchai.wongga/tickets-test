import { Button, makeStyles, Grid, Paper, Box } from "@material-ui/core";
import { Search } from "@material-ui/icons";

const useStyles = makeStyles((theme: any) => ({
  root: {
    "& .MuiGrid-spacing-xs-3 > .MuiGrid-item": {
      [theme.breakpoints.down("xs")]: {
        width: "100%",
      },
    },
  },
  paper: {
    backgroundColor: "#fff",
    height: "auto",
    marginBottom: 20,
  },
  searchBar: {
    display: "flex",
    justifyContent: "space-between",
    position: "relative",
    paddingRight: "240px",
    [theme.breakpoints.down("xs")]: {
      paddingRight: "0px",
    },
    "& .MuiFormControl-root": {
      padding: 0,
    },
    "& .MuiGrid-grid-sm-3": {
      marginBottom: 12,
    },
  },

  buttonBox: {
    textAlign: "center",
    position: "absolute",
    top: 0,
    right: 0,
    [theme.breakpoints.down("xs")]: {
      position: "relative",
    },
    "& button": {
      margin: "0px 5px",
      color: "#ffffff",
      height: "52px",
    },
  },
  buttonSearch: {
    marginTop: "0px",
  },
  buttonReset: {
    marginTop: "0px",
  },
}));

interface IFilters {
  children: any;
  visionFilter?: boolean;
  visionButtonCreate?: boolean;
  visionButtonDelete?: boolean;
  onCreate?: () => void;
  onDelete?: () => void;
  onSearch?: () => void;
}

const FilterCommon = (props: IFilters) => {
  const { children, visionFilter = false, onSearch } = props;
  const classes = useStyles();

  return (
    <Paper className={classes.paper}>
      <Grid className={classes.searchBar} container>
        {visionFilter && (
          <>
            <Grid item sm={12}>
              <Grid container spacing={3}>
                {children}
              </Grid>
            </Grid>
            <Box className={classes.buttonBox}>
              <Button
                className={classes.buttonSearch}
                startIcon={<Search />}
                color="primary"
                variant="contained"
                onClick={onSearch}
              >
                Search
              </Button>

              <Button
                className={classes.buttonReset}
                color="secondary"
                variant="contained"
                type="reset"
              >
                Reset
              </Button>
            </Box>
          </>
        )}
      </Grid>
    </Paper>
  );
};

export default FilterCommon;
