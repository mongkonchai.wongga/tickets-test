import { call } from "redux-saga/effects";
import { AxiosRequestConfig } from "axios";
import axios from "../services/axios.service";

interface IAxiosService {
  url: string;
  headers?: AxiosRequestConfig;
}

// Axios service
const axiosService = (config: IAxiosService) => {
  if (process.env.NODE_ENV === "production") {
    console.log = () => {};
  }

  const url: string = config.url;
  if (!url) {
    throw new Error("Axios: Invalid URL");
  }

  if (config.headers) {
    axios.defaults.headers = {
      ...axios.defaults.headers,
      ...(config.headers as any),
    };
  }

  return axios;
};

// --------- Get ---------
export const callGet = (url: string, params?: any, headers?: any) =>
  call(() =>
    axiosService({ url, headers })
      .get(url, { params: params })
      .then((response) => response.data)
      .catch((error: { response: { data: any } }) => {
        throw error.response?.data || error;
      })
  );
// --------- Patch ---------
export const callPatch = (url: string, data?: any, headers?: any) =>
  call(() =>
    axiosService({ url, headers })
      .patch(url, data)
      .then((response) => response.data)
      .catch((error: { response: { data: any } }) => {
        throw error.response?.data || error;
      })
  );

// --------- Post ---------
export const callPost = (url: string, data?: any, headers?: any) =>
  call(() =>
    axiosService({ url, headers })
      .post(url, data)
      .then((response) => response.data)
      .catch((error: { response: { data: any } }) => {
        throw error.response?.data || error;
      })
  );

// --------- Put ---------
export const callPut = (url: string, data?: any, headers?: any) =>
  call(() =>
    axiosService({ url, headers })
      .put(url, data)
      .then((response) => response.data)
      .catch((error: { response: { data: any } }) => {
        throw error.response?.data || error;
      })
  );

// --------- Delete ---------
export const callDelete = (url: string, data?: any, headers?: any) =>
  call(() =>
    axiosService({ url, headers })
      .delete(url, { data: data })
      .then((response) => response.data)
      .catch((error: { response: { data: any } }) => {
        throw error.response?.data || error;
      })
  );
