import "./App.css";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import TicketForm from "./pages/ticketForm";
import ListTicket from "./pages/listTicket";
import { Provider } from "react-redux";
import { store } from "./stores";

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Routes>
          <Route path="/" element={<ListTicket />} />
          <Route path="/form" element={<TicketForm />} />
          <Route path="/form/:id" element={<TicketForm />} />
        </Routes>
      </Router>
    </Provider>
  );
}

export default App;
