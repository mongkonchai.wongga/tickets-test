import { combineReducers } from "redux";
import ticketReducer, { ITicketState } from "./ticket/ticket.reducer";
export const rootPersist = ["authenReducer", "generalReducer"];
export const authPersist = ["authenReducer"];

export interface IStates {
  ticketReducer: ITicketState;
}

const rootReducer = combineReducers({
  ticketReducer,
});

export default rootReducer;
