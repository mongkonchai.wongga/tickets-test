import { all } from 'redux-saga/effects';
import ticketSaga from './ticket/ticket.saga';

const rootSaga = function* () {
  yield all([
    ...ticketSaga
  ]);
};

export default rootSaga;
