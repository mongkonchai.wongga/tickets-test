/* eslint-disable import/no-anonymous-default-export */
import { IActionReducer } from "../../services/action.reducer";
import { TicketAction } from "./ticket.action";

const TicketState = {
  list: [],
  form: {},
  list_status: [],
  total: 0,
} as ITicketState;

export interface ITicketState {
  list: any[];
  list_status: any[];
  form: any;
  total: 0;
}

export default (state = TicketState, e: IActionReducer) => {
  switch (e.type) {
    case TicketAction.STATUS_LIST_S: {
      const { payload } = e;
      return { ...state, list_status: payload };
    }

    case TicketAction.TICKET_LIST_S: {
      const { payload } = e;
      return { ...state, list: payload.result, total: payload.total };
    }

    case TicketAction.TICKET_FORM_S: {
      const { payload } = e;
      return { ...state, form: payload };
    }

    default: {
      return state;
    }
  }
};
