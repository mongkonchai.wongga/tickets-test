/* eslint-disable import/no-anonymous-default-export */
import { put, takeLatest } from "redux-saga/effects";
import { ActionReducer } from "../../services/action.reducer";
import { IActionSaga } from "../../services/action.saga";
import { callGet, callPost, callPut } from "../../services/call-api";
import { TicketAction } from "./ticket.action";

const host = process.env.REACT_APP_API_BACKEND;

function* listStatusSaga(e: IActionSaga) {
  try {
    const response: any[] = yield callGet(`${host}/tickets/status`);
    yield put(
      ActionReducer({ type: TicketAction.STATUS_LIST_S, payload: response })
    );
    e.onSuccess();
  } catch (err) {
    e.onFailure(err);
  }
}

function* listTicketSaga(e: IActionSaga) {
  const { payload } = e;
  try {
    const response: any[] = yield callGet(`${host}/tickets`, payload);
    yield put(
      ActionReducer({ type: TicketAction.TICKET_LIST_S, payload: response })
    );
    e.onSuccess();
  } catch (err) {
    e.onFailure(err);
  }
}

function* ticketFormSaga(e: IActionSaga) {
  const { payload } = e;
  try {
    const response: any[] = yield callGet(`${host}/tickets/${payload}`);
    yield put(
      ActionReducer({ type: TicketAction.TICKET_FORM_S, payload: response })
    );
    e.onSuccess();
  } catch (err) {
    e.onFailure(err);
  }
}

function* ticketStoreSaga(e: IActionSaga) {
  const { payload } = e;
  try {
    yield callPost(`${host}/tickets`, payload);
    e.onSuccess();
  } catch (err) {
    e.onFailure(err);
  }
}

function* ticketUpdateSaga(e: IActionSaga) {
  const { id, ...data } = e.payload;
  try {
    yield callPut(`${host}/tickets/${id}`, data);
    e.onSuccess();
  } catch (err) {
    e.onFailure(err);
  }
}

export default [
  takeLatest(TicketAction.TICKET_LIST_R, (e: IActionSaga) => listTicketSaga(e)),
  takeLatest(TicketAction.TICKET_FORM_R, (e: IActionSaga) => ticketFormSaga(e)),
  takeLatest(TicketAction.TICKET_STORE, (e: IActionSaga) => ticketStoreSaga(e)),
  takeLatest(TicketAction.STATUS_LIST_R, (e: IActionSaga) => listStatusSaga(e)),
  takeLatest(TicketAction.TICKET_UPDATE, (e: IActionSaga) =>
    ticketUpdateSaga(e)
  ),
];
